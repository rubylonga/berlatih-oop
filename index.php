<?php

 require('animal.php');
require('hewan.php');

$sheep = new Animal('shaun');
 
echo "name : $sheep->name <br>";
echo "legs : $sheep->legs <br>";
echo "cold blooded : $sheep->cold_blooded <br> <br>";

$kodok = new Frog('buduk');
echo "name : $kodok->name <br>";
echo "legs : $kodok->legs <br>";
echo "cold blooded : $kodok->cold_blooded <br>";
echo "jump : ";
$kodok->jump();
echo "<br> <br>";

$sungokong = new Ape('keras sakti');
echo "name : $sungokong->name <br>";
echo "legs : $sungokong->legs <br>";
echo "cold blooed : $sungokong->cold_blooded <br>";
echo "yell : "; 
$sungokong->yell();
