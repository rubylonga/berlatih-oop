<?php

require_once('animal.php');

class Ape extends Animal{
    public $legs = 2;

    public function yell(){
        echo "Auooo";
    }
}

class Frog extends Animal
{
    public function jump()
    {
        echo "Hop Hop";
    }
}